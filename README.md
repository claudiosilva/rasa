### Instalação

[Instalação no Ubuntu linux](https://rasa.com/docs/rasa/installation/)

```
mkdir rasa && cd rasa
sudo apt update
sudo apt install python3-venv python3-dev python3-pip -y
python3 -m venv ./venv
source ./venv/bin/activate
sudo -H pip3 install --upgrade pip
pip3 install rasa
```

[Appendix]()
```
pip3 install rasa-x --extra-index-url https://pypi.rasa.com/simple
pip3 install tensorflow
sudo apt-get install python3-ujson -y
```

### Testar a instalação
```
rasa --help
rasa --version
```

### Primeiro projeto no Rasa

```
rasa init # Criar o projeto
Please enter a path where the project will be created [default: current directory] .
Directory '/home/cadu/Documentos/rasa' is not empty. Continue? Yes
Do you want to train an initial model? 💪🏽 Yes
Do you want to speak to the trained assistant on the command line? 🤖 Yes
Do you want to speak to the trained assistant on the command line? 🤖 Yes
```

### Interagindo
- Deve ser executado o comando para voltar ao ambiente `source ./venv/bin/activate` caso não executar, não veremos o executário do rasa.
- Para sair do chat `/stop`   
- Para conectar novamente, devemos estar no diretório e executar `rasa shell`

### Rasa X
```
sudo apt-get install pkg-config libxml2-dev libxmlsec1-dev libxmlsec1-openssl -y
pip3 install rasa-x --extra-index-url https://pypi.rasa.com/simple
```

- Ir até o arquivo `credentials.yml` descomentar e adicionar o socketio
- A página index.html deve estar na raiz.
- apos isso rodar o comando:
```
rasa run --cors "*" --debug
```
Em seguida, abrir o local do index.html via navegador e testar o chatbot.


### Publicar em um site real
- Acessar e baixar https://startbootstrap.com/theme/freelancer
- Abrir o arquivo index.html e inserir no body:
```
<div id="rasa-chat-widget" data-websocket-url="http://localhost:5005/"></div>
<script src="https://unpkg.com/@rasahq/rasa-chat" type="application/javascript"></script>
```
- Acessar a URL do bootstrap e já estará funcionando o chatbot

[Fonte:](https://rasa.com/docs/rasa/connectors/your-own-website/)
[Otima fonte](https://dadosaocubo.com/como-criar-um-chatbot-com-rasa-open-source/)

### CDD Conversation-Driven Development

Desenvolvimento Orientado à Conversação

(CDD) é o processo de ouvir seus usuários e usar esses insights para melhorar seu assistente de IA. É a abordagem de melhores práticas abrangente para o desenvolvimento de chatbots.

[Fonte](https://rasa.com/docs/rasa/conversation-driven-development)


### NLU (Natural Language Understanding)
NLU (Natural Language Understanding) é a parte do Rasa Open Source que realiza classificação de intenção, extração de entidade e recuperação de resposta.

O NLU receberá uma frase de uma interação com usuario.

### Entendendo

1. nlu.yml = Entendimento de linguagem natural, onde o usuário digita algo.

2. domain.yml = A resposta para algo da nlu estará em domain.yml, deve declarar a intenção e adicionar um texto.

3. stories.yml = Deve ser adicionado uma história, por exemplo caminho feliz e caminho triste. No stories pode ser adicionado mais de uma intenção/ação


# Rasa Actions

- O arquivo para funcionar as ações personalizadas é  `actions/actions.yml`. Deve ser adicionado as ações aqui.
- No arquivo `endpoints.yml` deve ser descomentado:


- Treinar o Rasa, executar o Rasa Actions e por fim o chat do Rasa. 
```
rasa train
rasa run actions
rasa run cors "*" --debug
```

# Mensagem personalizada.

#### **data/nlu.yml**
```
- intent: msg_personalizada
  examples: |
    - ação personalizada
```

#### **domain.yml**
```
intents:
  - msg_personalizada

actions:
  - action_msg_personalizada
```

#### **data/stories.yml**
```
- story: msg personalizada
  steps:
  - intent: msg_personalizada
  - action: action_msg_personalizada
```

#### **actions.py**
```
from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher

class ActionHelloWorld(Action):

    def name(self) -> Text:
        return "action_msg_personalizada"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        dispatcher.utter_message(text=f"Ola mundo das ações personalizadas")

        return []
```
#### **endspoints.yml**
```

action_endpoint:
  url: "http://localhost:5055/webhook"
```

# Slot

#### **nlu.yml**
```
nlu:
- intent: perguntar_tamanho_camisa
  examples: |
    - pergunte o tamanho da minha camisa
- intent: repetir_tamanho_camisa
  examples: |
    - qual tamanho da minha camisa
- intent: diga_tamanho_camisa
  examples: |
    - [pequeno](shirt_size)
    - [medio](shirt_size)
    - [grande](shirt_size)
```

#### **domain.yml**
```
intents:
- perguntar_tamanho_camisa
- diga_tamanho_camisa
- repetir_tamanho_camisa

entities:
- shirt_size
slots:
  shirt_size:
    type: text
    influence_conversation: true
    mappings:
    - type: from_entity
      entity: shirt_size

  utter_perguntar_tamanho_camisa:
  - text: Qual o tamanho da sua camisa, pequeno, medio ou grande?
  utter_lembrar:
  - text: Obrigado. Vou me lembrar disso.

actions:
- acao_dizer_tamanho_camisa
- utter_perguntar_tamanho_camisa
- utter_lembrar
```

#### **stories.yml**
```
stories:

- story: Perguntar o tamanho da camisa
  steps:
  - intent: perguntar_tamanho_camisa
  - action: utter_perguntar_tamanho_camisa
  - intent: diga_tamanho_camisa
  - action: utter_lembrar

- story: Repetir o 
  steps:
  - intent: repetir_tamanho_camisa
  - action: acao_dizer_tamanho_camisa
```

#### **actions.py**
```
from typing import Any, Text, Dict, List

from rasa_sdk.events import SlotSet
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher


class ActionSayShirtSize(Action):

    def name(self) -> Text:
        return "acao_dizer_tamanho_camisa"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        shirt_size = tracker.get_slot("shirt_size")
        if not shirt_size:
            dispatcher.utter_message(text="Não sei o tamanho da sua camisa.")
        else:
            dispatcher.utter_message(text=f"O tamanho da sua camisa é {shirt_size}!")
        return []
```

#### **endpoints.yml**
```
action_endpoint:
  url: "http://localhost:5055/webhook"
```


# Form para texto

#### **nlu.yml**
```
nlu:
- intent: solicitar_nomes
  examples: |
    - eu quero te dar meu nome
    - dê o nome
    - você precisa saber meu nome
    - você sabe meu nome?
```

#### **domain.yml**
```
intents:
- solicitar_nomes

forms:
  nome_formulario:
    required_slots:
      - primeiro_nome
      - ultimo_nome
slots:
  primeiro_nome:
    type: text
    influence_conversation: true
    mappings:
      - type: from_text
        conditions:
         - active_loop: nome_formulario
           requested_slot: primeiro_nome
  ultimo_nome:
    type: text
    influence_conversation: true
    mappings:
      - type: from_text
        conditions:
         - active_loop: nome_formulario
           requested_slot: ultimo_nome

actions:
- utter_slots_valores
- utter_submit

responses:
  utter_ask_primeiro_nome:
  - text: Qual é o seu primeiro nome?
  utter_ask_ultimo_nome:
  - text: Então, {primeiro_nome}, qual é o seu sobrenome?
  utter_submit:
  - text: Ok. Obrigado!
  utter_slots_valores:
  - text: Vou lembrar que seu nome é {primeiro_nome} {ultimo_nome}!
```

#### **rules.yml**
```
rules:

- rule: ativar form
  steps:
  - intent: solicitar_nomes
  - action: nome_formulario
  - active_loop: nome_formulario

- rule: submeter form
  condition:
  - active_loop: nome_formulario
  steps:
  - action: nome_formulario
  - active_loop: null
  - slot_was_set:
    - requested_slot: null
  - action: utter_submit
  - action: utter_slots_valores
```

# Cria botões no chat
#### **data/nlu.yml**
```
nlu:
- intent: botoes
  examples: |
    - Me mostre botoes
```

#### **domain.yml**
```
intents:
  - botoes
  - botao_bom
  - botao_ruim

responses:
  utter_botoes:
  - text: "O que você achou"
    buttons:
    - title: Bom
      payload: /botao_bom
    - title: ruim
      payload: /botao_ruim
  utter_botao_bom:
  - text: "Muito agradecido pelo feedback positivo."
  utter_botao_ruim:
  - text: "Obrigado pelo feedback, vamos procurar melhorar"
```

#### **stories.yml**
```
stories:

- story: botao bom
  steps:
  - intent: botoes
  - action: utter_botoes
  - intent: botao_bom
  - action: utter_botao_bom

- story: botao ruim
  steps:
  - intent: botoes
  - action: utter_botoes
  - intent: botao_ruim
  - action: utter_botao_ruim
```

# Integrar no Twilio

#### **credentials.yml**
```
rest:
    twilio:
      account_sid: 
      auth_token: 
      twilio_number: "whatsapp:"
```

Acessar de acordo com as imagens abaixo para localizar as informações acima:
![imagem1](imagens/imagem1.png)
![imagem2](imagens/imagem2.png)


Para funcionar a intetração com o RASA, é precisa que a URL seja **HTTPS**. Caso não tenha, utilizar o NGROK com comando abaixo:
```
ngrok http 5005
```

Copiar a linha gerada de HTTPS do NGROK ex:
```
Forwarding                    https://cab1-191-135-26-74.sa.ngrok.io -> http://localhost:5005
```

Inserir este endereço https no twilio acrescida de /webhooks/twilio/webhook ex: https://cab1-191-135-26-74.sa.ngrok.io/webhooks/twilio/webhook
![imagem3](imagens/imagem3.png)


Inserir o Twilio Number na sua lista e procurá-lo no WhatsApp, após localizar, insira o código conforme apresentado no print acima **join time-step**

Iniciar o Rasa
```
rasa run -m models --enable-api
```

[Referencia](https://www.youtube.com/watch?v=K7boxP8Q50M&ab_channel=DroidCity)


# Integrar no facebook

https://developers.facebook.com/apps/

- Clicar em criar aplicativo
- Selecionar em Empresa
- Inserir nome, por exemplo rasa.
- Concluir

Parte 2
- Selecionar Messenger e clicar em configurar
- Clicar em adicionar ou remover páginas.
- Criar uma Página
- Gerar Token
```
EAAS5YBI9WgcBAJHK7q1h58J1ZB3KSJY6om8OFwborPzUcAtWcbEdaFYLGtOKsLI217ZCZA9ZCLty4rw2GjMDRhxwQ70UvhYt7iSqeWbnPu2YfKr2eZCVXIMR4tCMmGmeSoSGLN1J7YjEps0RwLtN3zv5i19FcbEZCLaXzbCA6xznG10xuNjaNy
```
- Acessar Configurações -> Básico e coletar o **Display Name:**, também coletar **Chave Secreta do Aplicativo**, em seguida inserir no arquivo **credentials.yml**.
```
facebook:
  verify: "rasa"
  secret: "afe1996ffaa53e2d634f71076b613f52"
  page-access-token: "EAAS5YBI9WgcBAJ4Gd7ilu5XRIxbClVGezNZAr4qgM4ZBjEXZBYuMpAJiYV6OFRiZBbqvq0ZAeFuPZAdFH4iHrDf0FVNpLCuA7p0kTe45yKhudL3ukTffhh55cMvaGLD4OyyzOczg6mtxXyCIdyH6w8Et7C31RjunKjs1XqDItJAmSlDypRoZCL9"
```

- Treinar e executar o rasa com o comando:
```
rasa train
rasa run -m models --enable-api --cors "*"
```

- Iniciar o NGROK e coletar a URL com o protocolo HTTPS.
```
ngrok http 5005
```

- Acessar **Messenger** -> **Configurações** e em Webhooks, Adicionar URL de retorno do NGROK, inserindo no final /webhooks/facebook/webhook
```
https://d4cd-191-135-26-74.sa.ngrok.io/webhooks/facebook/webhook
```
- Em **Token de verificação**, insira algo, por exemplo, *rasa*.

- 
