### Instalação

[Instalação no Ubuntu linux](https://rasa.com/docs/rasa/installation/)

```
mkdir rasa && cd rasa
sudo apt update
sudo apt install python3-venv python3-dev python3-pip -y
python3 -m venv ./venv
source ./venv/bin/activate
sudo -H pip3 install --upgrade pip
pip3 install rasa
```

[Appendix]()
```
pip3 install rasa-x --extra-index-url https://pypi.rasa.com/simple
pip3 install tensorflow
sudo apt-get install python3-ujson -y
```

### Testar a instalação
```
rasa --help
rasa --version
```

### Primeiro projeto no Rasa

```
rasa init # Criar o projeto
Please enter a path where the project will be created [default: current directory] .
Directory '/home/cadu/Documentos/rasa' is not empty. Continue? Yes
Do you want to train an initial model? 💪🏽 Yes
Do you want to speak to the trained assistant on the command line? 🤖 Yes
Do you want to speak to the trained assistant on the command line? 🤖 Yes
```

### Interagindo
- Deve ser executado o comando para voltar ao ambiente `source ./venv/bin/activate` caso não executar, não veremos o executário do rasa.
- Para sair do chat `/stop`   
- Para conectar novamente, devemos estar no diretório e executar `rasa shell`



### Rasa X
```
sudo apt-get install pkg-config libxml2-dev libxmlsec1-dev libxmlsec1-openssl -y
pip3 install rasa-x --extra-index-url https://pypi.rasa.com/simple
```

- Ir até o arquivo `credentials.yml` descomentar e adicionar o socketio
- A página index.html deve estar na raiz.
- apos isso rodar o comando:
```
rasa run --cors "*" --debug
```
Em seguida, abrir o local do index.html via navegador e testar o chatbot.


### Publicar em um site real
- Acessar e baixar https://startbootstrap.com/theme/freelancer
- Abrir o arquivo index.html e inserir no body:
```
<div id="rasa-chat-widget" data-websocket-url="http://localhost:5005/"></div>
<script src="https://unpkg.com/@rasahq/rasa-chat" type="application/javascript"></script>
```
- Acessar a URL do bootstrap e já estará funcionando o chatbot

[Fonte:](https://rasa.com/docs/rasa/connectors/your-own-website/)